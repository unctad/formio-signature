export default () => {
  return {
    components: [
      {
        type: 'tabs',
        key: 'tabs',
        components: [
          {
            key: 'display',
            label: 'Display',
            weight: 0,
            components: [
              {
                weight: 0,
                type: 'textfield',
                input: true,
                key: 'label',
                label: 'Label',
                placeholder: 'Button Label',
                tooltip: 'The label for this button',
                validate: {
                  required: true,
                },
              },
            ],
          },
          {
            key: 'data',
            label: 'Data',
            weight: 0,
            components: [
              {
                weight: 0,
                type: 'textfield',
                input: true,
                key: 'backendUrl',
                label: 'Backend URL',
                validate: {
                  required: true,
                },
              },
              {
                type: 'panel',
                title: 'Signing',
                collapsible: false,
                components: [
                  {
                    weight: 0,
                    type: 'select',
                    input: true,
                    key: 'method',
                    label: 'Method',
                    dataSrc: "values",
                    defaultValue: 'idcard',
                    data: {
                      values: [
                        {
                          label: 'ID-Card',
                          value: 'idcard',
                        },
                        {
                          label: 'Mobile-ID',
                          value: 'mID',
                        },
                        {
                          label: 'Smart-ID',
                          value: 'smartid',
                        },
                      ],
                    },
                    validate: {
                      onlyAvailableItems: true,
                      required: true,
                    },
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'documentName',
                    label: 'Document name',
                    tooltip: 'Name of the signed document',
                    validate: {
                      required: true,
                    },
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'containerName',
                    label: 'Container name',
                    tooltip: 'Name of the signed document container',
                    validate: {
                      required: true,
                    },
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'document',
                    label: 'Document',
                    tooltip: 'Base64 file to sign',
                    validate: {
                      required: true,
                    },
                  },
                ],
              },
              {
                type: 'panel',
                title: 'Signer',
                collapsible: false,
                components: [
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'country',
                    label: 'Country',
                    tooltip:
                      '2 character country code used for Smart-ID signing',
                  },
                  {
                    weight: 0,
                    type: 'select',
                    input: true,
                    key: 'language',
                    label: 'Language',
                    tooltip: 'Language for Mobile-ID signing',
                    dataSrc: "values",
                    data: {
                      values: [
                        {
                          label: 'EST',
                          value: 'EST',
                        },
                        {
                          label: 'ENG',
                          value: 'ENG',
                        },
                        {
                          label: 'RUS',
                          value: 'RUS',
                        },
                        {
                          label: 'LIT',
                          value: 'LIT',
                        },
                      ],
                    },
                    validate: {
                      onlyAvailableItems: true,
                    },
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'personIdentifierCode',
                    label: 'Person identifier code',
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'phoneNr',
                    label: 'Phone nr',
                  },
                  {
                    weight: 0,
                    type: 'textfield',
                    input: true,
                    key: 'messageToDisplay',
                    label: 'Message to display',
                    tooltip:
                      "Text displayed on user's device during Mobile-ID and Smart-ID signing",
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };
};

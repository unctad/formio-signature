// eslint-disable-next-line @typescript-eslint/ban-ts-comment

import * as Converter from 'base64-blob';

import SiGaClient from './SiGa/SiGaClient';
import MobileIdSigningInfo from './SiGa/model/MobileIdSigningInfo';
import SmartIdSigningInfo from './SiGa/model/SmartIdSigningInfo';

import editForm from './editForm/DigitalSignature.editForm';

interface Constructor<T> {
  new(component: any, options: any, data: any): T;
}

interface ButtonComponentBase {
  component: any;
  loadRefs: (element: any, refs: any) => any;

  attach(element: any): any;

  refs: any;
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default function DigitalSignatureClass<T extends ButtonComponentBase>(
    SuperClass: Constructor<T>
) {
  class DigitalSignature extends (<Constructor<ButtonComponentBase>>(
      SuperClass
  )) {
    private siga: SiGaClient;

    public constructor(component: any, options: any, data: any) {
      super(component, options, data);
      this.siga = new SiGaClient(this.component.backendUrl);
    }

    public static schema(...extend: object[]) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return super.schema(
          {
            type: 'DigitalSignature',
            label: 'Sign',
            key: 'DigitalSignature',
          },
          ...extend
      );
    }

    public static get builderInfo() {
      return {
        title: 'Digital Signature',
        group: 'basic',
        icon: 'fa fa-pencil',
        weight: 110,
        schema: DigitalSignature.schema(),
      };
    }

    public static editForm = editForm;

    public onClick() {
      Converter.base64ToBlob(this.getDocumentAsDataUrl())
          .then(document => {
            switch (this.component.method) {
              case 'idcard':
                return this.siga.remoteSign(
                    document,
                    this.component.documentName,
                    this.component.containerName
                );
              case 'mID':
                return this.siga
                    .beginMobileIdSigningFlow(
                        document,
                        this.component.documentName,
                        this.component.containerName,
                        this.component.personIdentifierCode,
                        this.component.phoneNr,
                        this.component.language,
                        this.component.messageToDisplay
                    )
                    .then((info: MobileIdSigningInfo) => {
                      this.displayChallengeId(info.challengeId);
                      return this.siga.endMobileIdSigningFlow(
                          info.containerId,
                          info.signatureId
                      );
                    });
            case 'smartid':
              return this.siga
                  .beginSmartIdSigningFlow(
                      document,
                      this.component.documentName,
                      this.component.containerName,
                      this.component.personIdentifierCode,
                      this.component.country,
                      this.component.messageToDisplay
                  )
                  .then((info: SmartIdSigningInfo) => {
                    this.displayChallengeId(info.challengeId);
                    return this.siga.endSmartIdSigningFlow(
                        info.containerId,
                        info.signatureId
                    );
                  });
          }
        })
        .then(signedDocument => {
          return Converter.blobToBase64(signedDocument);
        })
        .then(documentAsDataUrl => {
          this.setDocument(documentAsDataUrl.split(',')[1]);
          this.displayContainerDownload();
        })
        .catch((error: Error) => {
          console.log(error);
        })
        .finally(() => {
          this.hideChallengeId();
        });
    }

    public attach(element: Element) {
      if (element) {
        const link = document.createElement('a');
        link.innerHTML = 'Download';
        link.download = this.component.containerName;
        link.style.display = 'none';
        link.setAttribute('ref', 'containerDownload');
        element.append(link);

        const challenge = document.createElement('p');
        challenge.style.display = 'none';
        challenge.setAttribute('ref', 'challenge');
        element.append(challenge);

        this.loadRefs(element, {
          containerDownload: 'single',
          challenge: 'single',
        });
      }
      return super.attach(element);
    }

    private displayChallengeId(id: string) {
      const challenge = this.refs.challenge;
      challenge.innerHTML = id;
      challenge.style.display = 'inherit';
    }

    private hideChallengeId() {
      const challenge = this.refs.challenge;
      challenge.innerHTML = '';
      challenge.style.display = 'none';
    }

    private displayContainerDownload() {
      const link = this.refs.containerDownload;
      link.href = this.getDocumentAsDataUrl();
      link.style.display = 'inherit';
    }

    private setDocument(document: string) {
      this.component.document = document;
    }

    private getDocumentAsDataUrl() {
      return `data:application/octet-stream;base64,${this.component.document}`;
    }
  }

  return DigitalSignature;
}

import HwcCertificate from './model/HwcCertificate';
import HwcSignature from './model/HwcSignature';
import MobileIdSigningInfo from './model/MobileIdSigningInfo';
import SmartIdSigningInfo from './model/SmartIdSigningInfo';

import CreateContainerResponse from './model/http/CreateContainerResponse';
import PrepareMobileIdSigningResponse from './model/http/PrepareMobileIdSigningResponse';
import PrepareRemoteSigningResponse from './model/http/PrepareRemoteSigningResponse';
import PrepareSmartIdSigningResponse from './model/http/PrepareSmartIdSigningResponse';

import {AxiosResponse} from 'axios';

const axios = require('axios');
const hwcrypto = require('hwcrypto-js');
const Base64 = require('base64-js');
const FormData = require('form-data');

export default class SiGaClient {
  public constructor(public url: string) {}

  public remoteSign(file: object, fileName: string, containerName: string) {
    let containerId: string;
    let certificate: HwcCertificate;
    let signatureId: string;
    return this.createContainer(file, fileName, containerName)
      .then((data: CreateContainerResponse) => {
        containerId = data.id;
        return this.getCertificate();
      })
      .then((hwcCertificate: HwcCertificate) => {
        certificate = hwcCertificate;
        return this.prepareRemoteSigning(
          containerId,
          Array.prototype.slice.call(certificate.encoded)
        );
      })
      .then((data: PrepareRemoteSigningResponse) => {
        signatureId = data.generatedSignatureId;
        const dataToSign = new Uint8Array(
          Base64.toByteArray(data.dataToSignHash)
        );
        return this.signHash(
          containerId,
          certificate,
          data.digestAlgorithm,
          dataToSign
        );
      })
      .then((signature: HwcSignature) => {
        return this.finalizeRemoteSigning(
          containerId,
          signatureId,
          Array.prototype.slice.call(signature.value)
        );
      })
      .then(() => {
        return this.downloadContainer(containerId);
      });
  }

  public beginMobileIdSigningFlow(
    file: object,
    fileName: string,
    containerName: string,
    personIdentifier: string,
    phoneNr: string,
    language: string,
    messageToDisplay: string
  ) {
    let containerId: string;
    return this.createContainer(file, fileName, containerName)
      .then((data: CreateContainerResponse) => {
        containerId = data.id;
        return this.prepareMobileIdSigning(
          containerId,
          personIdentifier,
          phoneNr,
          language,
          messageToDisplay
        );
      })
      .then((data: PrepareMobileIdSigningResponse) => {
        return new Promise<MobileIdSigningInfo>(resolve => {
          resolve(
            new MobileIdSigningInfo(
              containerId,
              data.generatedSignatureId,
              data.challengeId
            )
          );
        });
      });
  }

  public endMobileIdSigningFlow(containerId: string, signatureId: string) {
    return this.finalizeMobileIdSigning(containerId, signatureId).then(() => {
      return this.downloadContainer(containerId);
    });
  }

  public beginSmartIdSigningFlow(
    file: object,
    fileName: string,
    containerName: string,
    personIdentifier: string,
    country: string,
    messageToDisplay: string
  ) {
    let containerId: string;
    return this.createContainer(file, fileName, containerName)
      .then((data: CreateContainerResponse) => {
        containerId = data.id;
        return this.prepareSmartIdSigning(
          containerId,
          personIdentifier,
          country,
          messageToDisplay
        );
      })
      .then((data: PrepareSmartIdSigningResponse) => {
        return new Promise<SmartIdSigningInfo>(resolve => {
          resolve(
            new SmartIdSigningInfo(
              containerId,
              data.generatedSignatureId,
              data.challengeId
            )
          );
        });
      });
  }

  public endSmartIdSigningFlow(containerId: string, signatureId: string) {
    return this.finalizeSmartIdSigning(containerId, signatureId).then(() => {
      return this.downloadContainer(containerId);
    });
  }

  public createContainer(
    file: object,
    fileName: string,
    containerName: string
  ) {
    const data = new FormData();
    data.append('file', file, fileName);
    data.append('containerName', containerName);
    return axios
      .post(`${this.url}/container`, data)
      .then(
        (response: AxiosResponse) => response.data as CreateContainerResponse
      );
  }

  public downloadContainer(containerId: string) {
    return axios
      .get(`${this.url}/container/${containerId}`, {
        responseType: 'blob',
      })
      .then((response: AxiosResponse) => response.data as object);
  }

  public prepareRemoteSigning(containerId: string, certificate: number[]) {
    const data = {
      containerId: containerId,
      certificate: certificate,
    };
    return axios
      .post(`${this.url}/signing/remote/prepare`, data)
      .then(
        (response: AxiosResponse) =>
          response.data as PrepareRemoteSigningResponse
      );
  }

  public finalizeRemoteSigning(
    containerId: string,
    signatureId: string,
    signature: number[]
  ) {
    const data = {
      containerId: containerId,
      signatureId: signatureId,
      signature: signature,
    };
    return axios.post(`${this.url}/signing/remote/finalize`, data);
  }

  public prepareMobileIdSigning(
    containerId: string,
    personIdentifier: string,
    phoneNr: string,
    language: string,
    messageToDisplay: string
  ) {
    const data = {
      containerId: containerId,
      personIdentifier: personIdentifier,
      phoneNr: phoneNr,
      language: language,
      messageToDisplay: messageToDisplay,
    };
    return axios
      .post(`${this.url}/signing/mobile-id/prepare`, data)
      .then(
        (response: AxiosResponse) =>
          response.data as PrepareMobileIdSigningResponse
      );
  }

  public finalizeMobileIdSigning(containerId: string, signatureId: string) {
    const data = {
      containerId: containerId,
      signatureId: signatureId,
    };
    return axios.post(`${this.url}/signing/mobile-id/finalize`, data);
  }

  public prepareSmartIdSigning(
    containerId: string,
    personIdentifier: string,
    country: string,
    messageToDisplay: string
  ) {
    const data = {
      containerId: containerId,
      personIdentifier: personIdentifier,
      country: country,
      messageToDisplay: messageToDisplay,
    };
    return axios
      .post(`${this.url}/signing/smart-id/prepare`, data)
      .then(
        (response: AxiosResponse) =>
          response.data as PrepareSmartIdSigningResponse
      );
  }

  public finalizeSmartIdSigning(containerId: string, signatureId: string) {
    const data = {
      containerId: containerId,
      signatureId: signatureId,
    };
    return axios.post(`${this.url}/signing/smart-id/finalize`, data);
  }

  public getCertificate() {
    return hwcrypto.getCertificate({lang: 'en'});
  }

  public signHash(
    containerId: string,
    certificate: HwcCertificate,
    digestAlgorithm: string,
    dataToSign: Uint8Array
  ) {
    const hash = {
      type: digestAlgorithm,
      value: dataToSign,
    };
    const options = {};
    return hwcrypto.sign(certificate, hash, options);
  }
}

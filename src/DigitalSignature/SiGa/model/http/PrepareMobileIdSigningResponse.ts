export default class PrepareMobileIdSigningResponse {
  constructor(
    public generatedSignatureId: string,
    public challengeId: string
  ) {}
}

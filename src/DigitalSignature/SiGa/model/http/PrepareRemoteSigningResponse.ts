export default class PrepareRemoteSigningResponse {
  constructor(
    public generatedSignatureId: string,
    public dataToSignHash: string,
    public digestAlgorithm: string
  ) {}
}

export default class PrepareSmartIdSigningResponse {
  constructor(
    public generatedSignatureId: string,
    public challengeId: string
  ) {}
}

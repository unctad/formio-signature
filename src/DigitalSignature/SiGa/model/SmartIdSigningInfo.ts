export default class SmartIdSigningInfo {
  constructor(
    public containerId: string,
    public signatureId: string,
    public challengeId: string
  ) {}
}

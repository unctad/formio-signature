export default class MobileIdSigningInfo {
  constructor(
    public containerId: string,
    public signatureId: string,
    public challengeId: string
  ) {}
}
